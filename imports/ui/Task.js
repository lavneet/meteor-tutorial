import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import classnames from 'classnames';
 
// Task component - represents a single todo item
export default class Task extends Component {
  toggleChecked() {
    Meteor.call('tasks.update', this.props.task._id, !this.props.task.checked);
  }

  delete() {
    Meteor.call('tasks.remove', this.props.task._id);
  }

  togglePrivate() {
    Meteor.call('tasks.setPrivate', this.props.task._id, !this.props.task.private);
  }

  render() {
    // different className for checked tasks.
    const taskClassName = classnames({
      checked: this.props.task.checked,
      private: this.props.task.private,
    });


    return (
      <li className={taskClassName} >
        <input type="checkbox" readOnly 
        checked={!!this.props.task.checked} 
        onClick={this.toggleChecked.bind(this)}>
        </input>
        <span className="text">
          {this.props.task.text} <strong>({this.props.task.username})</strong>
        </span>
        { this.props.showPrivateButton ?
          <button className="toggle-private" onClick={this.togglePrivate.bind(this)}>
            {this.props.task.private ? 'Private' : 'Public'}
          </button> : '' }
        <button className="delete" onClick={this.delete.bind(this)}>
          &times;
        </button>
      </li>
    );
  }
}